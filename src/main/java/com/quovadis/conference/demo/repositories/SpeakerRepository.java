package com.quovadis.conference.demo.repositories;

import com.quovadis.conference.demo.models.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
}
