package com.quovadis.conference.demo.repositories;

import com.quovadis.conference.demo.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {
}
